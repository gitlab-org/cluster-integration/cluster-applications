FROM "registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:helm-3.17.1-kube-1.32.2-alpine-3.21.3"

ARG TARGETARCH
ARG HELMFILE_VERSION="0.162.0"
ARG KUSTOMIZE_VERSION="4.5.4"
ARG HELM_DIFF_VERSION="3.9.11"
ARG HELM_GIT_VERSION="0.10.0"

RUN wget https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_${TARGETARCH}.tar.gz \
  && tar xf helmfile_${HELMFILE_VERSION}_linux_${TARGETARCH}.tar.gz \
  && mv helmfile /usr/local/bin/helmfile \
  && chmod u+x /usr/local/bin/helmfile

RUN wget -qO- https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_${TARGETARCH}.tar.gz | tar -xzv -C /usr/local/bin

RUN apk add --no-cache bash

RUN helm plugin install https://github.com/databus23/helm-diff --version ${HELM_DIFF_VERSION}

RUN helm plugin install https://github.com/aslafy-z/helm-git.git --version ${HELM_GIT_VERSION}

COPY src/ /opt/cluster-applications/

RUN ln -s /opt/cluster-applications/bin/* /usr/local/bin/
